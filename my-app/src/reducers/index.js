export default function(state = "choose page", action){
	switch(action.type){
		case "PAGE_SELECTED":
			return action.payload;
			
		default:
			return state;
	}
}