import React, {Component} from 'react'
import './style.css'
import {connect} from 'react-redux'

class Page extends Component{

render (){ 
		return(
			<div className = 'page'>
				<h1>{this.props.text}</h1>
			</div>
		)
	}
}

function mapStateToProps(state){
	return{
		text: state
	}
}
			
export default connect (mapStateToProps)(Page)

