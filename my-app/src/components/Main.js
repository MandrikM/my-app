import React from "react";
import Header from './Header'
import Menu from './Menu'
import Page from './Page'

function Main() {
  return (
      	<div className='app-wrapper'>
      		<Header />
      		<Menu />
      		<Page />			
      	</div>
    );
}

export default Main;


