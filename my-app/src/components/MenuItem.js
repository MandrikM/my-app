import './style.css'
import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {select} from '../actions/index'

class MenuItem extends Component{

	render(){
		return( 
			<div>
				<button  onClick= {() => this.props.select('PAGE_ASYNC',this.props.page.name)} >
					Page to {this.props.page.name}
				</button>
				<hr/>
			</div>
		);
	}
} 

function mapDispatchToProps(dispatch){
	return bindActionCreators({select: select},dispatch)
}

export default connect(null, mapDispatchToProps )(MenuItem)