import React, {Component} from 'react'
import './style.css'
import MenuItem from './MenuItem'
import {pages} from './Pages.json'

class Menu extends Component{

	render(){
		return(
		<div className ='menu'>
			{pages.map((page) =>{
			return(
				<MenuItem page = {page}/>
				)
			})}
		</div>
		);
	}
}

export default Menu

