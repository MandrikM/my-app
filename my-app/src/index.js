import React from 'react';
import ReactDOM from 'react-dom';
import Main from './components/Main'
import {Provider} from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import reducer from './reducers/index'
import createSagaMiddleware from 'redux-saga'
import pageAsync from './components/sagas'

const sagaMiddleware = createSagaMiddleware()
const store = createStore(reducer, applyMiddleware(sagaMiddleware))

sagaMiddleware.run(pageAsync)

function render(){
	ReactDOM.render(
	<Provider store ={store}>
		<Main />
	</Provider>,
document.getElementById('root')
	); 
}

render()
store.subscribe(render)
